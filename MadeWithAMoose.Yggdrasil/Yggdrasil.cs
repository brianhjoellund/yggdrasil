﻿namespace MadeWithAMoose.Yggdrasil
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;

    public class Yggdrasil
    {
        public static void Start<TType>(string[] argv) where TType: Yggdrasil
        {
            if (!IsHelpCommand(argv))
                return;

            if(argv.Length == 2)
                PrintGenericHelpForType<TType>();
            else
                PrintHelpForCommand<TType>(argv);
        }

        private static void PrintHelpForCommand<TType>(string[] argv)
        {
            string command = argv[2];

            var method = typeof (TType).GetMethod(command, BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.IgnoreCase);

            if (method == null)
            {
                Console.WriteLine("{0} is an unknown command", command);
            }
            else
            {
                Console.Write("{0} ", Path.GetFileNameWithoutExtension(argv[0]));
                Console.Write(GetSyntaxForCommand(method));

                var description = GetDescriptionForCommand(method);

                if(description != null)
                    Console.WriteLine("\t{0}", description);
                else
                    Console.WriteLine();

                var options = GetOptionsForCommand(method);

                if(options != null)
                    foreach (var option in options)
                    {
                        Console.Write("--{0}", option.Name);
                        if(option.ShortTag != default(char))
                            Console.Write(", -{0}", option.ShortTag);
                        Console.Write("\t");
                        
                        if(!String.IsNullOrWhiteSpace(option.Description))
                            Console.WriteLine(option.Description);
                        else
                            Console.WriteLine();
                    }
            }
        }

        private static string GetSyntaxForCommand(MethodInfo method)
        {
            var syntax = new StringBuilder();
            syntax.Append(method.Name);

            var parameters = method.GetParameters();

            if (parameters.Length > 0)
            {
                foreach (var parameter in parameters)
                    if (parameter.IsOptional)
                        syntax.AppendFormat(" [{0}]", parameter.Name);
                    else
                        syntax.AppendFormat(" <{0}>", parameter.Name);
            }

            return syntax.ToString();
        }

        private static string GetDescriptionForCommand(MethodInfo method)
        {
            var descriptionAttribute = method.GetCustomAttribute<DescriptionAttribute>();

            if (descriptionAttribute == null)
                return null;

            return descriptionAttribute.Description;
        }

        private static IEnumerable<OptionAttribute> GetOptionsForCommand(MethodInfo method)
        {
            var optionAttributes = method.GetCustomAttributes<OptionAttribute>();

            return optionAttributes;
        }

        private static bool IsHelpCommand(string[] argv)
        {
            if (argv.Length < 2)
                return false;

            return argv[1] == "help";
        }

        private static void PrintGenericHelpForType<TType>()
        {
            var type = typeof (TType);
            var description = type.GetCustomAttribute<DescriptionAttribute>();

            if (description != null)
                Console.WriteLine(description.Description);
            else
                Console.WriteLine("No help found for {0}", typeof (TType).Name);

            foreach (var method in type.GetMethods(BindingFlags.Instance|BindingFlags.Public|BindingFlags.DeclaredOnly))
            {
                Console.Write("\t{0}", GetSyntaxForCommand(method));
                string methodDescription = GetDescriptionForCommand(method);

                if(description != null)
                    Console.Write("\t{0}", methodDescription);
                
                Console.WriteLine();
            }
        }
    }
}
