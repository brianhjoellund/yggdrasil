﻿namespace MadeWithAMoose.Yggdrasil
{
    using System;

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class OptionAttribute : Attribute
    {
        private readonly string name;
        private readonly char shortTag;
        private Type type = typeof(string);

        public OptionAttribute(string name)
        {
            this.name = name;
        }

        public OptionAttribute(string name, char shortTag) : this(name)
        {
            this.shortTag = shortTag;
        }

        public string Name
        {
            get { return name; }
        }

        public char ShortTag
        {
            get { return shortTag; }
        }

        public bool IsRequired { get; set; }

        public string Description { get; set; }

        public Type Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}
