﻿namespace MadeWithAMoose.Yggdrasil.Tests
{
    using System;

    [Description("A test mockup class for Yggdrasil")]
    class YggdrasilMock : Yggdrasil
    {
        [Option("important", 'i', IsRequired = false, Type = typeof(bool), Description = "Is the printed message important")]
        [Option("color", 'c', Description = "The color, in RRGGBB format")]
        [Description("Prints a message to the console")]
        public void Print(string message)
        {
            Console.WriteLine(message);
        }
    }
}
