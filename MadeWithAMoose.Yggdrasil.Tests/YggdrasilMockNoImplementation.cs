﻿namespace MadeWithAMoose.Yggdrasil.Tests
{
    class YggdrasilMockNoImplementation : Yggdrasil
    {
        public void Print(string message)
        { }
    }
}
