﻿namespace MadeWithAMoose.Yggdrasil.Tests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using NUnit.Framework;

    [TestFixture]
    class YggdrasilTests
    {
        private const string ProgramPath = @"C:\Program Files\Yggdrasil\yggdrasil.exe";

        [Test]
        public void CallingHelpWithNoArgumentOnATypeWithNoCommandsShouldPrintAnErrorMessage()
        {
            string message = ExecuteAndReturnConsoleOut<EmptyType>(new[] {ProgramPath, "help"});
            Assert.That(message, Is.EqualTo("No help found for EmptyType\r\n"));
            Debug.WriteLine(message);
        }

        [Test]
        public void CallingHelpWithNoArgumentShouldPrintTheTypesDescriptionAlongWithHelpForAllCommonCommands()
        {
            string message = ExecuteAndReturnConsoleOut<YggdrasilMock>(new[] {ProgramPath, "help"});
            Assert.That(message, Is.EqualTo("A test mockup class for Yggdrasil\r\n\tPrint <message>\tPrints a message to the console\r\n"));
            Debug.WriteLine(message);
        }

        private static string ExecuteAndReturnConsoleOut<TType>(string[] argv) where TType : Yggdrasil
        {
            using (var writer = new StringWriter())
            {
                Console.SetOut(writer);
                Yggdrasil.Start<TType>(argv);

                return writer.ToString();
            }
        }

        [Test]
        public void CallingHelpOnACommandThatDoesNotHaveADescriptionJustPrintsItsSyntax()
        {
            string message = ExecuteAndReturnConsoleOut<YggdrasilMockNoImplementation>(new[] {ProgramPath, "help", "print"});
            Assert.That(message, Is.EqualTo("yggdrasil Print <message>\r\n"));
            Debug.WriteLine(message);
        }

        [Test]
        public void CallingHelpOnPrintShouldPrintAppropiateHelpMessage()
        {
            string message = ExecuteAndReturnConsoleOut<YggdrasilMock>(new[] {ProgramPath, "help", "print"});
            Assert.That(message, Is.EqualTo("yggdrasil Print <message>\tPrints a message to the console\r\n"));
            Debug.WriteLine(message);
        }
    }
}
